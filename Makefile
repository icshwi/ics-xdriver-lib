CCFLAGS := -Wall -O3 -std=gnu11 -g -ggdb
LDFLAGS := -Wall -O3

TARGETS := libxildrv.so libxildrv.a xil-rw

all: $(TARGETS)

clean:
	@$(RM) -f $(TARGETS)

xil-rw: xil-rw.o libxildrv.a
	$(CC) $(LDFLAGS) $^ -o $@

%.so: %.o
	$(CC) $(LDFLAGS) -fPIC -shared $^ -o $@

%.a: %.o
	ar rc $@ $^

%.o: %.cpp
	$(CC) -c $(CCFLAGS) -fPIC $< -o $@

%.o: %.c
	$(CC) -c $(CCFLAGS) -fPIC $< -o $@
