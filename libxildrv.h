#ifndef LIBXILDRV_H_
#define LIBXILDRV_H_

#include <stdint.h>

struct xilinx_device_struct;
typedef struct xilinx_device_struct xildev;

#ifdef __cplusplus
extern "C" {
#endif

xildev * xil_open_device(const char *device_base_name);
int xil_close_device(xildev *dev);
unsigned int xil_get_bar_size(xildev *dev);
int xil_get_c2h_fd(xildev *dev, unsigned int channel);
int xil_get_h2c_fd(xildev *dev, unsigned int channel);
int xil_read_reg(xildev *dev, uint32_t address, uint32_t *value);
int xil_write_reg(xildev *dev, uint32_t address, uint32_t value);

#ifdef __cplusplus
}
#endif

#endif /* LIBXILDRV_H_ */
