#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <getopt.h>
#include <errno.h>

#include "libxildrv.h"

bool ends_with(const char *str, const char *end)
{
	const char *s = str, *e = end;
	//find respective ends
	while(*s) s++;
	while(*e) e++;
	if(e - end > s - str)
		//ending longer than the string
		return false;
	while(e >= end)
	{
		if(*e != *s)
			return false;
		e--;
		s--;
	}
	return true;
}

void print_usage(char * prog_name)
{
	printf("ESS Xilinx Driver register read/write utility\n");
	printf("Read : %s <dev_name> -r <address> [<count>]\n", prog_name);
	printf("Write: %s <dev_name> -w <address> <value>\n", prog_name);
	printf("Check ESSFFW component IDs and versions: %s -i\n", prog_name);
	printf("\n");
	printf("Example: %s /dev/xdma0 -r 0xC01000 16\n", prog_name);
	printf("\n");
}

#define OPER_NONE		0
#define OPER_READ		1
#define OPER_WRITE		2
#define OPER_IDENT		3

int main(int argc, char* argv[])
{
	xildev * dev = NULL;
	int opt, result = -1;
	int oper = OPER_NONE;

	while((opt = getopt (argc, argv, "hrwi")) != -1)
	{
		if(oper != OPER_NONE)
		{
			fprintf(stderr, "Please specify only one operation\n");
			return -1;
		}
		switch(opt)
		{
			case 'h':
				print_usage(argv[0]);
				return 0;
			case 'r':
				oper = OPER_READ;
				break;
			case 'w':
				oper = OPER_WRITE;
				break;
			case 'i':
				oper = OPER_IDENT;
				break;
			default:		/* '?' */
				print_usage(argv[0]);
				return -1;
		}
	}

	if(oper == OPER_NONE)
	{
		fprintf(stderr, "Please specify the operation to be performed\n");
		return -1;
	}

	int extra_args = argc - optind;
	if(extra_args > 0)
	{
		char * file_name = strdup(argv[optind]);
		if(ends_with(file_name, "_user"))
			file_name[strlen(file_name) - 5] = '\0';
		dev = xil_open_device(file_name);
		free(file_name);
		if(!dev)
		{
			fprintf(stderr, "Cannot open device '%s' due to '%s'\n", argv[optind], strerror(errno));
			return -1;
		}
	}
	else
	{
		fprintf(stderr, "Device not specified\n");
		return -1;
	}
	
	unsigned int bar_size = xil_get_bar_size(dev);
	printf("User BAR size %d\n", bar_size);
	
	do
	{
		uint32_t address = 0;
		if(extra_args > 1)
		{
			address = strtoul(argv[optind + 1], NULL, 0);
			if(address & 3)
			{
				fprintf(stderr, "Address has to be divisible by 4\n");
				break;
			}
		}

		if(oper == OPER_READ)
		{
			if(extra_args < 2 || extra_args > 3)
			{
				fprintf(stderr, "Expected 2 or 3 positional arguments\n");
				break;
			}

			uint32_t count = (extra_args > 2) ? strtoul(argv[optind + 2], NULL, 0) : 1;
			for(int i = 0; i < count; i++)
			{
				uint32_t value;
				int ret = xil_read_reg(dev, address, &value);
				if(ret != 0)
				{
					printf("Cannot read register at 0x%08X - error: %s\n", address, strerror(errno));
					break;
				}
				printf("[0x%08X] = 0x%08X\n", address, value);
				address += 4;
			}
		}
		else if(oper == OPER_WRITE)
		{
			if(extra_args != 3)
			{
				fprintf(stderr, "Expected 3 positional arguments\n");
				break;
			}

			uint32_t value = strtoul(argv[optind + 2], NULL, 0);
			printf("[0x%08X] = 0x%08X\n", address, value);
			int ret = xil_write_reg(dev, address, value);
			if(ret != 0)
			{
				printf("Cannot write register at 0x%08X - error: %s\n", address, strerror(errno));
				break;
			}
		}
		else if(oper == OPER_IDENT)
		{
			int ret;
			uint32_t value;
			address = 0x000000;
			ret = xil_read_reg(dev, address, &value);
			if(ret != 0)
			{
				printf("Cannot read register at 0x%08X - error: %s\n", address, strerror(errno));
				break;
			}
			printf("Framework ID: 0x%X\n", value);

			address = 0x000004;
			ret = xil_read_reg(dev, address, &value);
			if(ret != 0)
			{
				printf("Cannot read register at 0x%08X - error: %s\n", address, strerror(errno));
				break;
			}
			printf("Framework version: %d.%d.%d\n", (value >> 24) & 0xFF, (value >> 8) & 0xFFFF, value & 0xFF);

			address = 0x000010;
			ret = xil_read_reg(dev, address, &value);
			if(ret != 0)
			{
				printf("Cannot read register at 0x%08X - error: %s\n", address, strerror(errno));
				break;
			}
			printf("Project ID: 0x%X\n", value);

			address = 0x000014;
			ret = xil_read_reg(dev, address, &value);
			if(ret != 0)
			{
				printf("Cannot read register at 0x%08X - error: %s\n", address, strerror(errno));
				break;
			}
			printf("Project build number: %d\n", value);

			address = 0x000018;
			ret = xil_read_reg(dev, address, &value);
			if(ret != 0)
			{
				printf("Cannot read register at 0x%08X - error: %s\n", address, strerror(errno));
				break;
			}
			printf("Project GIT hash: %08x\n", value);

			struct {
				const char * name;
				uint32_t base_addr;
			} components[] = {
				{"BSP"}, {"RSP"}, {"FSP1"}, {"FSP2"}, {"CSTMLOG"}
			};
			
			int num_components = sizeof(components) / sizeof(components[0]);
			for(int comp_idx = 0; comp_idx < num_components; comp_idx++)
			{
				address = 0x000100 + comp_idx * 4;
				ret = xil_read_reg(dev, address, &components[comp_idx].base_addr);
				if(ret != 0)
				{
					printf("Cannot read register at 0x%08X - error: %s\n", address, strerror(errno));
					break;
				}
				if(components[comp_idx].base_addr != 0xFFFFFFFF)
				{
					address = components[comp_idx].base_addr;
					ret = xil_read_reg(dev, address, &value);
					if(ret != 0)
					{
						printf("Cannot read register at 0x%08X - error: %s\n", address, strerror(errno));
						break;
					}
					printf("%s ID: 0x%X\n", components[comp_idx].name, value);
					
					address += 4;
					ret = xil_read_reg(dev, address, &value);
					if(ret != 0)
					{
						printf("Cannot read register at 0x%08X - error: %s\n", address, strerror(errno));
						break;
					}
					printf("%s version: %d.%d.%d\n", components[comp_idx].name,
						   (value >> 24) & 0xFF, (value >> 8) & 0xFFFF, value & 0xFF);
				}
			}
		}
		
		result = 0;
	} while(0);
	
	xil_close_device(dev);
	return result;
}

